import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginPageComponent} from './auth/login-page/login-page.component';
import {NotFoundComponent} from './shared/components/not-found/not-found.component';
import {SystemComponent} from './system/system.component';
import {AdminGuard} from './shared/guards/admin-guard.service';
import {AuthGuard} from './shared/guards/auth-guard.service';


const routes: Routes = [
    {
      path: 'system', component: SystemComponent, children: [
        {
          path: 'admin',
          loadChildren: () => import('./system/admin-system/create-edit-users.module').then(m => m.CreateEditUsersModule),
          canActivate: [AdminGuard]
        },
        {
          path: 'users',
          loadChildren: () => import('./system/user-system/overview-users.module').then(m => m.OverviewUsersModule),
          canActivate: [AuthGuard]
        },
        {path: '', redirectTo: 'users', pathMatch: 'full'},
      ]
    },
    {path: '', redirectTo: 'system', pathMatch: 'full'},
    {path: 'login', component: LoginPageComponent},
    {path: '**', component: NotFoundComponent}
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
