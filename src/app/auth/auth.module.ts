import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {LoginPageComponent} from './login-page/login-page.component';
import {SharedModule} from '../shared/shared.module';
import {MatCardModule} from '@angular/material';


@NgModule({
  declarations: [
    LoginPageComponent
  ],
    imports: [
        CommonModule,
        SharedModule,
        TranslateModule,
        MatCardModule,
    ]
})
export class AuthModule { }
