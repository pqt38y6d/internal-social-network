import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthService} from '../../shared/services/auth.service';
import {User} from '../../shared/models/user';


@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.sass']
})

export class LoginPageComponent implements OnInit {

  user = new User();


  constructor(private authService: AuthService) {
  }

  ngOnInit() {
  }


  onSubmit(contactForm: NgForm) {
    this.authService.login(contactForm.form.value);
  }

}
