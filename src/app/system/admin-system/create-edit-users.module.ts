import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CreateEditUsersRoutingModule} from './create-edit-users-routing.module';
import {MatDatepickerModule, MatNativeDateModule} from '@angular/material';
import {SharedModule} from '../../shared/shared.module';
import {TranslateModule} from '@ngx-translate/core';
import {FileSizePipe} from '../../shared/pipes/file-size.pipe';
import {AdmUserInfComponent} from './adm-user-inf/adm-user-inf.component';
import {MainDialogComponent} from '../../shared/components/main-dialog/main-dialog.component';


@NgModule({
  declarations: [
    AdmUserInfComponent,
    MainDialogComponent,
    FileSizePipe,
  ],
  imports: [
    CommonModule,
    CreateEditUsersRoutingModule,
    SharedModule,
    MatDatepickerModule,
    MatNativeDateModule,
    TranslateModule,
  ]
})
export class CreateEditUsersModule { }
