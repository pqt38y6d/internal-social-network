import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormArray, FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {MainDialogComponent} from '../../../shared/components/main-dialog/main-dialog.component';
import {ToastrService} from 'ngx-toastr';
import {User} from '../../../shared/models/user';
import {UsersService} from '../../../shared/services/users.service';
import {TranslateService} from '@ngx-translate/core';


@Component({
  selector: 'app-adm-user-inf',
  templateUrl: './adm-user-inf.component.html',
  styleUrls: ['./adm-user-inf.component.sass'],
})
export class AdmUserInfComponent implements OnInit {

  user = new User();
  newUser = new User();
  id: number = this.activateRoute.snapshot.params.id;
  startDate = new Date();
  endDate = new Date();

  maxStartEducationYear = new Date().getFullYear();
  maxEndEducationYear = new Date().getFullYear() + 5;

  userInformationForm = this.formBuilder.group({
    avatar: [''],
    email: ['', [
      Validators.required,
      Validators.email]
    ],
    password: ['', [
      Validators.required,
      Validators.minLength(6)]
    ],
    gender: ['Male', Validators.required],
    role: ['User', Validators.required],
    name: ['', [
      Validators.required,
      Validators.pattern('[a-zA-Z ]*')]
    ],
    surname: ['', [
      Validators.required,
      Validators.pattern('[a-zA-Z ]*')]
    ],
    birthday: [],
    education: this.formBuilder.array([
      this.createUniversityItem()
    ])
  });


  constructor(private activateRoute: ActivatedRoute,
              private router: Router,
              public usersService: UsersService,
              private formBuilder: FormBuilder,
              public dialog: MatDialog,
              private toastr: ToastrService,
              private translate: TranslateService) {
  }


  async ngOnInit() {
    this.endDate.setFullYear(this.endDate.getFullYear() - 100);
    this.startDate.setFullYear(this.startDate.getFullYear() - 18);

    if (this.id) {
      this.user = await this.usersService.getUserById(this.id).toPromise();
      this.setInitialValues(this.user);
    }
  }


  get education(): FormArray {
    return this.userInformationForm.get('education') as FormArray;
  }


  setInitialValues(user: User) {
    this.userInformationForm.patchValue(user);
    this.education.patchValue(user.education);
  };


  createUniversityItem(): FormGroup {
    return this.formBuilder.group({
      university: ['', Validators.required],
      faculty: ['', Validators.required],
      startYear: ['', [
        Validators.required,
        Validators.min(1950),
        Validators.max(this.maxStartEducationYear),
        Validators.pattern('^[0-9]*$')]],
      endYear: ['', [
        Validators.required,
        Validators.min(1950),
        Validators.max(this.maxEndEducationYear),
        Validators.pattern('^[0-9]*$')]]
    }, {validator: this.educationYearsRange});
  }

  addUniversity() {
    this.education.push(this.createUniversityItem());
  };

  removeUniversity(index: number) {
    this.education.removeAt(index);
  };


  onSubmit() {
    const dialogRef = this.dialog.open(MainDialogComponent, {
      panelClass: 'dialog-container',
      width: 'auto',
      height: 'auto',
      data: {data: this.user, message: this.id ? 'edit' : 'add'}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.newUser = this.userInformationForm.value;
      this.newUser.avatar = this.user.avatar;
      this.newUser.status = this.user.status;
      if (result) {
        if (!this.user.avatar) {
          this.user.avatar = 'https://i.mycdn.me/image?id=805736336005&plc=WEB&tkn=*Aa7tJ2GCNPkWL7E5M0oiD04fuXg&fn=sqr_288';
        }
        this.newUser.id = this.id;
        this.usersService.updateUser(this.newUser).subscribe(() => {
          this.router.navigate(['/system/admin']).then();
          this.translate.get('EDIT_PROFILE', {surname: this.user.surname}).subscribe((res: string) => {
            this.toastr.success(res);
          });
        });
      } else {
        if (!this.id) {
          this.newUser.status = 'offline';
          this.usersService.addUser(this.newUser).subscribe(() => {
            this.router.navigate(['/system/admin']).then();
            this.translate.get('CREATE_PROFILE', {surname: this.user.surname}).subscribe((res: string) => {
              this.toastr.success(res);
            });
          });
        }

      }
    });
  }


  openDialog(user: User) {
    this.dialog.open(MainDialogComponent, {
      panelClass: 'dialog-container',
      width: 'auto',
      height: 'auto',
      data: {data: user, message: 'editImage'}
    });
  }

  addAvatar(user: User) {
    this.dialog.open(MainDialogComponent, {
      width: 'auto',
      height: 'auto',
      data: {data: user, message: 'editAvatar'}
    });
  }


// education years range validator
  educationYearsRange: ValidatorFn = (formGroup: FormGroup) => {
    const start = +formGroup.get('startYear').value;
    const end = +formGroup.get('endYear').value;
    console.log(start, end);
    return start !== null && end !== null && start >= end
      ? {range: true}
      : null;
  }


}


