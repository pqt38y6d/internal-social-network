import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {MainPageComponent} from '../../shared/components/main-page/main-page.component';
import {AdmUserInfComponent} from './adm-user-inf/adm-user-inf.component';


const routes: Routes = [
    {path: '', component: MainPageComponent},
    {path: 'add', component: AdmUserInfComponent},
    {path: ':id', component: AdmUserInfComponent}
  ]
;

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateEditUsersRoutingModule {
}
