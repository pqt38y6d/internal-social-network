import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainPageComponent} from '../../shared/components/main-page/main-page.component';
import {UserInfComponent} from './user-inf/user-inf.component';

const routes: Routes = [
    {path: '', component: MainPageComponent},
    {path: ':id', component: UserInfComponent}
  ]
;

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class OverviewUsersRoutingModule {
}


