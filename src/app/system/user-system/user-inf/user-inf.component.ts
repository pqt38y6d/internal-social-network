import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {User} from '../../../shared/models/user';
import {UsersService} from '../../../shared/services/users.service';

@Component({
  selector: 'app-user-inf',
  templateUrl: './user-inf.component.html',
  styleUrls: ['./user-inf.component.sass']
})
export class UserInfComponent implements OnInit {

  user = new User();
  isUniversity: boolean;
  private id: number = this.activateRoute.snapshot.params.id;

  constructor(private activateRoute: ActivatedRoute,
              private usersService: UsersService) {
  }

  ngOnInit() {
    this.getUserInformation(this.id);
  }

  getUserInformation(id: number) {
    this.usersService.getUserById(id).subscribe(
      response => {
        this.user = response;
        this.user.education.length > 0 ? this.isUniversity = true : this.isUniversity = false;
      });
  }

}
