import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserInfComponent} from './user-inf/user-inf.component';
import {OverviewUsersRoutingModule} from './overview-users-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {MatCardModule} from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [
    UserInfComponent,
  ],
    imports: [
        CommonModule,
        OverviewUsersRoutingModule,
        SharedModule,
        MatCardModule,
        TranslateModule
    ]
})
export class OverviewUsersModule {
}
