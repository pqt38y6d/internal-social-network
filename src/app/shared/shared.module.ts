import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatButtonModule, MatCardModule,
  MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatPaginatorIntl,
  MatPaginatorModule,
  MatRadioModule,
  MatToolbarModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {AutocompleteSearchComponent} from './components/autocomplete-search/autocomplete-search.component';
import {StatusDirective} from './directives/status.directive';
import {AgePipe} from './pipes/age.pipe';
import {CustomMatPaginatorIntl} from './components/custom-mat-paginator/custom-mat-paginator';
import {MainPageComponent} from './components/main-page/main-page.component';
import {HeaderComponent} from './components/header/header.component';


@NgModule({
  declarations: [
    AutocompleteSearchComponent,
    StatusDirective,
    AgePipe,
    MainPageComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule,
    MatPaginatorModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule,
    MatDialogModule,
    FormsModule,
    MatRadioModule,
    MatAutocompleteModule,
    MatIconModule,
    RouterModule,
    MatMenuModule,
    TranslateModule,
    MatBadgeModule,
    MatToolbarModule,
    MatExpansionModule,
    MatListModule,
    MatCardModule,
  ],
  exports: [
    AutocompleteSearchComponent,
    MainPageComponent,
    HeaderComponent,
    MatPaginatorModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    MatDialogModule,
    ReactiveFormsModule,
    FormsModule,
    MatRadioModule,
    MatAutocompleteModule,
    MatIconModule,
    RouterModule,
    MatMenuModule,
    MatBadgeModule,
    StatusDirective,
    AgePipe,
    MatExpansionModule,
    MatListModule,
    MatCardModule,
  ],
  providers: [{
    provide: MatPaginatorIntl,
    useClass: CustomMatPaginatorIntl
  }]
})
export class SharedModule {
}
