import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../models/user';


@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private httpClient: HttpClient) {
  }

  getUserListByPage(page: number, size: number): Observable<HttpResponse<User[]>> {
    return this.httpClient.get<User[]>('users/', {
      params: new HttpParams().set('_page', page.toString()).set('_limit', size.toString()),
      observe: 'response'
    });
  }

  getUserById(id: number) {
    return this.httpClient.get<User>('users/' + id);
  }

  deleteUser(id: number) {
    return this.httpClient.delete('users/' + id);
  }

  updateUser(user: User) {
    return this.httpClient.put('users/' + user.id, user);
  }

  addUser(user: User) {
    return this.httpClient.post('users/', user);
  }

  autocompleteSearch(param: string) {
    return this.httpClient.get(`users?surname_like=${param}`);
  }


}
