import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../models/user';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {UsersService} from './users.service';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient,
              private router: Router,
              private toastr: ToastrService,
              private usersService: UsersService,
              private translate: TranslateService
  ) {
  }

  private isAuthenticated = true;
  private isAdmin = true;
  userLogined: User;


  getUserByEmail(email: string) {
    return this.httpClient.get('/users?email=' + email);
  }


  login(authFormData) {
    this.getUserByEmail(authFormData.email)
      .subscribe((user: User[]) => {
        if (user.length > 0) {
          this.userLogined = user[0];
          if (this.userLogined.password === authFormData.password) {
            this.userLogined.status = 'online';
            this.usersService.updateUser(this.userLogined).subscribe();
            localStorage.setItem('user', JSON.stringify(this.userLogined));
            if (this.userLogined.role === 'Administrator') {
              this.isAdmin = true;
              this.router.navigate(['/system/admin']);
            } else {
              this.router.navigate(['/system/users']);
            }
          } else {
            this.router.navigate(['/login']);
            this.translate.get('INVALID_PASSWORD').subscribe((res: string) => {
              this.toastr.error(res);
            });
          }
        } else {
          this.router.navigate(['/login']);
          this.translate.get('INVALID_LOGIN').subscribe((res: string) => {
            this.toastr.error(res);
          });
        }
      });
    this.isAuthenticated = true;
  }


  logout() {
    this.isAuthenticated = false;
    this.userLogined.status = 'offline';
    this.usersService.updateUser(this.userLogined).subscribe(() => {
      }
    );
    this.router.navigateByUrl('/login');
    localStorage.clear();
  }

  isLoggedIn() {
    return this.isAuthenticated;
  }

  isAdminIn() {
    return this.isAdmin;
  }
}




