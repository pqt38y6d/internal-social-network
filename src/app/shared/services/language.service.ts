import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  language = 'en';

  constructor(private translate: TranslateService) {
    translate.setDefaultLang('en');
  }


  setFR() {
    this.language = 'fr';
    this.translate.use(this.language);
  }

  setEN() {
    this.language = 'en';
    this.translate.use(this.language);
  }

}
