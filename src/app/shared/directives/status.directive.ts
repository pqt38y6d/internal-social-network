import {Directive, ElementRef, Input, OnChanges, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[appStatus]'
})
export class StatusDirective implements OnChanges{

  @Input() appStatus: string;
  color: string;


  constructor(private elementRef: ElementRef,
              private renderer: Renderer2) {
  }

  ngOnChanges() {
    if (this.appStatus === 'online') {
      this.color = 'green';
    } else {
      this.color = 'red';
    }
    this.renderer.setStyle(this.elementRef.nativeElement, 'backgroundColor', this.color);
  }
}
