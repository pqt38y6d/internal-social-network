import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'fileSize'
})
export class FileSizePipe implements PipeTransform {

  transform(value: any): string {
    if (value) {
      value = Number((value / 1000000).toFixed(3));
      return value;
    } else {
      return 'failed to count';
    }
  }

}
