export class User {
  id: number;
  name: string;
  surname: string;
  email: string;
  gender: string;
  avatar: string;
  education: Education[];
  password: string;
  birthday: Date;
  status?: string;
  role: string;
}


export class Education {
  university: string;
  faculty: string;
  startYear: number;
  endYear: number;
}
