import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, PageEvent} from '@angular/material';
import {MainDialogComponent} from '../main-dialog/main-dialog.component';
import {UsersService} from '../../services/users.service';
import {User} from '../../models/user';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-main-admin-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.sass']
})
export class MainPageComponent implements OnInit {
  users: User[] = [];
  dataLength: number;
  changePageEvent: PageEvent;
  defaultPageSize;
  defaultSizeOptions;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;


  constructor(public usersService: UsersService,
              public authService: AuthService,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    if (this.authService.isAdminIn()) {
      this.defaultPageSize = 5;
      this.defaultSizeOptions =  [5, 11 , 23];
    } else {
      this.defaultPageSize = 6;
      this.defaultSizeOptions =  [6, 12 , 24];
    }
    this.changePageEvent = new PageEvent();
    this.changePageEvent.pageIndex = 0;
    this.changePageEvent.pageSize = this.defaultPageSize;
    this.changePage(this.changePageEvent);
  }


  changePage($event: PageEvent) {
    this.changePageEvent = $event;
    this.changePageEvent.pageIndex += 1;
    this.usersService.getUserListByPage(this.changePageEvent.pageIndex, this.changePageEvent.pageSize).subscribe(
      response => {
        this.users = response.body;
        this.dataLength = Number(response.headers.get('X-Total-Count'));
      }
    );
  }


  openDialog(user: User) {
    const dialogRef = this.dialog.open(MainDialogComponent, {
      panelClass: 'dialog-container',
      width: 'auto',
      height: 'auto',
      data: {data: user, message: 'delete'}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.usersService.deleteUser(result).subscribe(() => {
        });
        this.usersService.getUserListByPage(this.changePageEvent.pageIndex, this.changePageEvent.pageSize).subscribe(
          response => {
            this.users = response.body;
            this.dataLength = Number(response.headers.get('X-Total-Count'));
          }
        );
      }
    });
  }

}
