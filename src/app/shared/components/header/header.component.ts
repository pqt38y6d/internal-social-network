import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {LanguageService} from '../../services/language.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  constructor(public authService: AuthService,
              public languageService: LanguageService,
              private router: Router
  ) {
  }


  ngOnInit() {
  }

  navigateToHome() {
    if (this.authService.isLoggedIn() && !this.authService.isAdminIn()) {
      this.router.navigateByUrl('system/users');
    } else {
      this.router.navigateByUrl('system/admin');
    }
  }
}
