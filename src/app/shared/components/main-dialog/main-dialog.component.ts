import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {User} from '../../models/user';

@Component({
  selector: 'app-deleting-dialog',
  templateUrl: './main-dialog.component.html',
  styleUrls: ['./main-dialog.component.sass'],
})
export class MainDialogComponent implements OnInit {

  fileData: any;
  previewUrl: any;

  constructor(public dialogRef: MatDialogRef<MainDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public dialog: MatDialog) {
  }


  ngOnInit() {
  }


  deleteAvatar(user: User)  {
    const dialogRef = this.dialog.open(MainDialogComponent, {
      panelClass: 'dialog-container',
      width: 'auto',
      height: 'auto',
      data: {data: user, message: 'deleteAvatar'}
    });
    this.dialogRef.close();
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        user.avatar = 'https://i.mycdn.me/image?id=805736336005&plc=WEB&tkn=*Aa7tJ2GCNPkWL7E5M0oiD04fuXg&fn=sqr_288';
      }
    });
  };


  editAvatar(user: User) {
    const dialogRef = this.dialog.open(MainDialogComponent, {
      panelClass: 'dialog-container',
      width: 'auto',
      height: 'auto',
      data: {data: user, message: 'editAvatar'}
    });
    this.dialogRef.close();
    dialogRef.afterClosed().subscribe();
  };

  fileProgress(fileInput: any) {
    this.fileData = fileInput.target.files[0] as File;
    this.preview();
  }


  preview() {
    const mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    const reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    // tslint:disable-next-line:variable-name
    reader.onload = (_event) => {
      this.previewUrl = reader.result;
    };
  }

  onSubmit(user: User) {
    if (this.previewUrl !== undefined) {
      user.avatar = this.previewUrl;
    }
    this.dialogRef.close();
  }
}
