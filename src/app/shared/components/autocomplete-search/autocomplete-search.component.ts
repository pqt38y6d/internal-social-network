import {Component, OnInit} from '@angular/core';
import {debounceTime, finalize, switchMap, tap} from 'rxjs/operators';
import {FormControl, Validators} from '@angular/forms';
import {UsersService} from '../../services/users.service';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-autocomplete-search',
  templateUrl: './autocomplete-search.component.html',
  styleUrls: ['./autocomplete-search.component.sass']
})
export class AutocompleteSearchComponent implements OnInit {
  searchCtrl = new FormControl('', Validators.pattern('[a-zA-Z0-9- ]*'));
  users: any;
  isLoading = false;

  constructor(
    private usersService: UsersService,
    private authService: AuthService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.searchCtrl.valueChanges
      .pipe(
        debounceTime(500),
        tap(() => {
          this.users = [];
          this.isLoading = true;
        }),
        switchMap(value =>
          this.usersService.autocompleteSearch(value)
            .pipe(
              finalize(() => {
                this.isLoading = false;
              }),
            )
        )
      )
      .subscribe(data => {
        this.users = data;
      });
  }

  navigateTo(id: number) {
    this.searchCtrl.reset();
    if (this.authService.isAdminIn()) {
      this.router.navigateByUrl(`system/admin/${id}`).then();
    } else {
      this.router.navigateByUrl(`system/users/${id}`).then();
    }
  }
}
