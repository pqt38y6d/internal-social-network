import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CreateEditUsersModule} from './system/admin-system/create-edit-users.module';
import {OverviewUsersModule} from './system/user-system/overview-users.module';
import {FooterComponent} from './shared/components/footer/footer.component';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MainDialogComponent} from './shared/components/main-dialog/main-dialog.component';
import {NgxSpinnerModule} from 'ngx-spinner';
import {AuthModule} from './auth/auth.module';
import {SharedModule} from './shared/shared.module';
import {CommonModule} from '@angular/common';
import {AuthService} from './shared/services/auth.service';
import {UsersService} from './shared/services/users.service';
import {AuthGuard} from './shared/guards/auth-guard.service';
import {AdminGuard} from './shared/guards/admin-guard.service';
import {ToastrModule} from 'ngx-toastr';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {SystemComponent} from './system/system.component';
import {NotFoundComponent} from './shared/components/not-found/not-found.component';
import {MatToolbarModule} from '@angular/material';
import {SpinnerInterceptorService} from './shared/services/spinner-interceptor.service';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/translate/', '.json');
}


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    SystemComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CreateEditUsersModule,
    OverviewUsersModule,
    AuthModule,
    SharedModule,
    HttpClientModule,
    NgxSpinnerModule,
    ToastrModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    MatToolbarModule,
  ],
  providers: [AuthService, UsersService, AuthGuard, AdminGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SpinnerInterceptorService,
      multi: true,
    }],
  entryComponents: [
    MainDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
