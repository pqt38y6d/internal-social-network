# SocialNetwork

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.20.

## Runnig Projest

Run custom script `npm run dev` for a start project. Then navigate to  `http://localhost:4200/`.

## Credentials

Administrator: 
    - Login: "mveitchp@biblegateway.com";
    - Password: "Taurine"
    
User:
    - Login: "tborwicko@globo.ru";
    - Password: "ewrewrtewrew"


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

